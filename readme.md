#RARE

This is an awesome Laravel 5 package.

##Installation instructions

Create a folder called _1-packages_ alongside your other website projects, it's prefixed with a 1 so it floats to the top. Clone the repo to a folder called _rare_  inside this so the structure will be _1-packages/rare_.

During development of the package it's quicker and easier to link to the development files residing on your on computer rather having to push to git and composer update every little change.

To do this add a _path repository_ to your Laravel project's composer.json pointing to the package as shown below.


	"repositories": [
		{
			"type": "path",
			"url": "../1-packages/rare"
		}
	]


Next add RARE to your required packages


	"require": {
		...
		"bwi/rare": "dev-develop"
	},


Now run `composer update` to make a reference between the project and the development package.

Any changes made to the package will be instantly reflected in the project including it with no need to push to git or run composer update.

Add the service provider to _app/config.php_.


	'providers' => [
		...
		Bwi\Rare\RareServiceProvider::class,
	]


Next publish the configuration file

`php artisan vendor:publish --provider="Bwi\Rare\RareServiceProvider"`

###DB Dumper

Run `composer require spatie/db-dumper` -- this will be added to the package composer file

Make sure your MySQL / MariaDB bin folder is on your system PATH.

###Laravel Activity logger

http://www.laravel-auditing.com/

Install `composer require spatie/laravel-activitylog`

	'providers' => [
		...
		Spatie\Activitylog\ActivitylogServiceProvider::class,
	],
	
Run `php artisan vendor:publish --provider="Spatie\Activitylog\ActivitylogServiceProvider" --tag="migrations"` then `php artisan migrate`


###PHP Coding Standards Fixer installation
PHP Coding Standards Fixer makes our generated code prettier. You only need to install this one as it is global - not for every project.
`composer global require friendsofphp/php-cs-fixer`
Make sure that your composer bin folder is on your path. It it’s not installed file generation will still succeed.
http://cs.sensiolabs.org/

###Email and final settings

Email will also need to be set up using MailGun or similar.

At the moment we need a couple of other things to:
* `composer require guzzlehttp/guzzle:~6.0` (if using MailGun)
* `composer require league/json-guard` (With my 'patch', see: https://github.com/thephpleague/json-guard/issues/104)

And you're good to go!

The schema readme is here: https://wearebwi.atlassian.net/wiki/display/RARE/RARE+Schema+Format


---

A guide to writing packages, covering the composer.json file, ServiceProviders etc can be found here: https://websanova.com/blog/laravel/creating-a-new-package-in-laravel-5-part-1-package-workflow

Writing tests for the package can be simplified with: http://orchestraplatform.com/docs/latest/components/testbench
