<?php

return [

	'schema_path' => database_path() . '/schemas',

	'models_path' => app_path() . '/Models',

	'controllers_path' => app_path() . '/Http/Controllers/',

	'views_path' => resource_path() . '/views/rare',

	'debug' => false,
];