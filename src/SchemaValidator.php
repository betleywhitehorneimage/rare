<?php

/*
 *
 * TODO !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! validation property needs validation!!!!!!!!!!!!!!!!!!!!!!!!!!!
 *
 *
 * */

namespace Bwi\Rare;

use Illuminate\Filesystem\Filesystem;
use League\JsonGuard\Dereferencer;
use League\JsonGuard\Validator;

class SchemaValidator
{
	private $fileSystem;
	private $schemaFile;
	protected $messages = [];

	public function __construct($schemaFile)
	{
		$this->fileSystem = new Filesystem;
		$this->schemaFile = $schemaFile;
	}

	public function validate($schemaRules = false) {
		if (!$schemaRules) {
			$schemaRules = json_decode($this->fileSystem->get(__DIR__ . '/../resources/validators/schema.json'));
		} else {
			$schemaRules = json_decode($this->fileSystem->get($schemaRules));
		}

		$json = json_decode($this->fileSystem->get($this->schemaFile));

		$deref  = new Dereferencer();
		$schemaRules = $deref->dereference($schemaRules);

		$validator = new Validator($json, $schemaRules);

		if ($validator->passes()) {
			echo 'Schema valid: ' . $this->schemaFile;

			return true;
		} else {
			echo 'Schema error: ' . $this->schemaFile;

			$errors = collect($validator->errors());

			$errors->each(function($item) {
				$this->error($item->toArray()['message'] . ' at ' . $item->toArray()['pointer']);
			});

			return false;
		}

	}

	/**
	 * Get the messages
	 *
	 * @return Illuminate\Support\Collection
	 */
	public function getMessages(){
		return collect($this->messages);
	}

	/**
	 * Output messages to the cli
	 */
	protected function info($message)
	{
		$this->messages[] = ['type' => 'info', 'text' => $message];
	}

	protected function error($message)
	{
		$this->messages[] = ['type' => 'error', 'text' => $message];
	}

	protected function debug($message)
	{
		$this->messages[] = ['type' => 'error', 'text' => $message, 'debug' => true];
	}
}