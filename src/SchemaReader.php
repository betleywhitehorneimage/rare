<?php
/*
Reads a JSON schema and allows us to get information about it such as fields,
field properties and relationships
*/

namespace Bwi\Rare;

use DB;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\Validator;


class SchemaReader
{
	protected $json = null;
	protected $databasePrefix;
	protected $names;

	/**
	 * The filesystem instance.
	 *
	 * @var \Illuminate\Filesystem\Filesystem
	 */
	protected $file;

	// TODO work out how to automatically inject the Filesystem object https://laravel.com/docs/5.3/container#automatic-injection
	public function __construct($schemaFile)
	{
		$fileSystem = new Filesystem;

		$this->databasePrefix = DB::getTablePrefix();


		// TODO $this->validateJson();
		$this->json = collect(json_decode($fileSystem->get($schemaFile), true));


		$this->setNames();

		//print_r($this->json);

	}

	/*private function validateJson() {
		// TODO make sure JSON is correctly formatted
	}*/

	/**
	 * Returns all the fields from a schema as a collection
	 *
	 * @return Illuminate\Support\Collection
	 */
	public function fields($timestamps = true) {
		$fields = collect($this->json['fieldGroups'])->flatMap(function ($fieldGroups) {
			return $fieldGroups['fields'];
		})->transform(function($item, $property) {
			return $this->checkField($item, $property);
		});

		if ($relationships = $this->relationships()) {
			$relationships->each(function ($item) use (&$fields) {
				if(isset($item['schema'])) {
					$fields[$item['schema']->name() . '_id'] = [
						'label' => $item['schema']->name(), // TODO get this name properly
						'dataType' => 'foreign-key'
					];
				}
			});
		}

		if ($timestamps) {
			if ($this->timestamps()) {
				$fields['created_at'] = [
					'dataType' => 'timestamp',
					'default' => '0000-00-00 00:00:00',
					'label' => 'Created at',
				];
				$fields['updated_at'] = [
					'dataType' => 'timestamp',
					'default' => '0000-00-00 00:00:00',
					'label' => 'Updated at',
				];
			}

			if ($this->softDeletes()) {
				// field
				$fields['deleted_at'] = [
					'dataType' => 'timestamp',
					'default' => 'null',
					'label' => 'Deleted at',
				];
			}
		}

		if ($this->getTrait('sluggable')) {
			$fields['slug'] = [
				'dataType' => 'varchar',
				'length' => 255,
				'label' => 'Slug',
			];
		}


		/*
		 * TODO set up defaults for fields - such as default length for varchar, default dataType for file or image uploads
		 * */

		return $fields;

	}

	public function validatedFields()
	{
		/*
		 * Define default rules that should be applied to datatypes
		 * */
		$defaultRules = [
			'bool' => 'boolean',
			'int' => 'numeric',
			'timestamp' => 'date',
			'varchar' => 'max:255'
		];

		$fields = collect($this->json['fieldGroups'])->flatMap(function ($fieldGroups) {
			return $fieldGroups['fields'];
		})->transform(function($field) use ($defaultRules) {
			$rules = [];

			// get all the default rules in a useful format
			if (array_key_exists($field['dataType'], $defaultRules)) {

				// add all the defaults into an array keyed by the validation rule
				collect(explode('|', $defaultRules[$field['dataType']]))->each(function($rule) use (&$rules) {
					$parts = explode(':', $rule);

					$rules[$parts[0]] = isset($parts[1]) ? $parts[1] : '';
				});
			}

			// do we already have validation rules?
			if (array_key_exists('validation', $field)) {
				// add or replace the rules with those from the validation
				collect(explode('|', $field['validation']['rules']))->each(function($rule) use (&$rules) {
					$parts = explode(':', $rule);
					$rules[$parts[0]] = isset($parts[1]) ? $parts[1] : '';
				});
			}

			// if a length was specified in the schema make sure this is used
			if (array_key_exists('length', $field)) {
				$rules['max'] = $field['length'];
			}

			// if field is marked as required then add the validation rule
			if (array_key_exists('required', $field) && $field['required'] === true) {
				$rules['required'] = '';
			}

			// reformat back to a string that the validator expects
			$rules = collect($rules)->transform(function($item, $key) {
				if ($item) {
					return $key . ':' . $item;
				} else {
					return $key;
				}
			})->implode('|');

			// add the validation and rules keys if they are missing
			if (array_key_exists($field['dataType'], $defaultRules) && !array_key_exists('validation', $field)) {
				$field['validation'] = [];
				$field['validation'] = ['rules' => []];

			}

			if (array_key_exists($field['dataType'], $defaultRules)) {
				$field['validation']['rules'] = $rules;
			}

			return $field;
		})->filter(function($item) {
			if (array_key_exists('validation', $item)) {
				return $item;
			}
		});

		return $fields;
	}

	/**
	 * Returns all the fields from a schema in their groups as a collection
	 *
	 * @return Illuminate\Support\Collection
	 */
	public function groupedFields() {
		return collect($this->json['fieldGroups']);
	}

	/**
	 * Sets up the defaults for fields if required
	 *
	 * @param $item
	 * @return mixed
	 */
	private function checkField($item, $property) {

		// TODO add these values to the configuration file

		// If the field has no length set in the schema but requires one then define it here
		$lengths = ['varchar' => 255];

		if (array_key_exists($item['dataType'], $lengths) && !array_key_exists('length', $item)) {
			$item['length'] = $lengths[$item['dataType']];
		}

		// If the field has no default set in the schema but requires one then define it here
		$defaults = ['bool' => 0];

		if (array_key_exists($item['dataType'], $defaults) && !array_key_exists('default', $item)) {
			$item['default'] = $defaults[$item['dataType']];
		}

		if (!array_key_exists('label', $item)) {
			$item['label'] = title_case($property);
		}

		return $item;
	}


	/**
	 * Gets name of the schema
	 *
	 * @return string
	 * */
	public function name($key = 'initial') {
		return $this->names[$key];
	}

	/**
	 * Gets name of the database table that will be used for this schema
	 *
	 * @return string
	 * */
	public function tableName() {
		return $this->databasePrefix . str_plural($this->json['name']);
	}

	/**
	 * Gets Schema.org type from the JSON schema
	 *
	 * @return string
	 * */
	public function schemaOrgType() {
		return $this->json['schemaOrgType'];
	}


	/**
	 * Gets an array of the guarded fields
	 *
	 * @return Collection|boolean
	 * */
	public function guardedFields() {

		if (array_key_exists('guarded', $this->json['properties'])) {
			return collect($this->json['properties']['guarded']);
		}

		return false;
	}


	/**
	 * Gets an array of the guarded fields
	 *
	 * @return Collection|boolean
	 * */
	public function getTrait($trait) {
		if (array_key_exists('traits', $this->json)) {
			if (array_key_exists($trait, $this->json['traits'])) {
				return collect($this->json['traits'][$trait]);
			}
		}

		return false;
	}


	/**
	 * Gets an array of the guarded fields
	 *
	 * @return Collection|boolean
	 * */
	public function softDeletes() {

		if (array_key_exists('softDeletes', $this->json['properties'])) {
			return $this->json['properties']['softDeletes'];
		}

		return false;
	}


	/**
	 * Gets an array of the guarded fields
	 *
	 * @return Collection|boolean
	 * */
	public function properties() {

		if (isset($this->json['properties'])) {
			return collect(['public' => $this->json['properties']['public'], 'protected' => $this->json['properties']['protected']]);
		}

		return false;
	}


	/**
	 * See if the timestamps (created_at, updated_at) is required
	 *
	 * @return Collection|boolean
	 * */
	public function timestamps() {

		if (array_key_exists('public', $this->json['properties'])) {
			return $this->json['properties']['public']['timestamps'];
		}

		return false;
	}

	/**
	 * Returns all the fields that use timestamp datatypes. These will be mutated in carbon objects etc
	 *
	 * */

	// TODO is it ineffecient to call the fields method again as it checks for relationships etc? Should we be storing this?
	public function dates() {
		$dates = [];

		// pass $dates in by reference so we can alter them in the closure - &$date
		$this->fields()->each(function ($item, $key) use (&$dates) {
			if ($item['dataType'] == 'timestamp') {
				$dates[] = $key;
			}
		});

		return $dates;
	}


	/**
	 * Returns all the relationships used by this model
	 */
	public function relationships()
	{
		if (isset($this->json['relationships'])) {
			$relationships = collect($this->json['relationships']);

			$relationships->transform(function ($item) {
				if(isset($item['schema'])) {
					$relationshipSchema = config('bwi-rare.schema_path') . '/' . str_singular($item['schema']) . '.json';
					$relationshipSchemaReader = new SchemaReader($relationshipSchema);

					$item = array_merge($item, ['schema' => $relationshipSchemaReader]);
				}
				return $item;
			});

			return $relationships;
		}

		return false;
	}

	/**
	 * Set all the variations on the schema name needed for the builders, upper, lower, studly
	 * We use them in a lot of places to let’s store them in the object rather than calling
	 * string manipulation methods all over the place
	 */
	protected function setNames() {
		$this->names = array_dot([
			'initial' => $this->json['name'],
			'slug' => str_slug($this->json['name']),
			'plural' => [
				'camel' => str_plural(camel_case($this->json['name'])),
				'lower' => str_plural(strtolower($this->json['name'])),
				'snake' => str_plural(snake_case($this->json['name'])),
				'studly' => str_plural(studly_case($this->json['name'])),
				'upper' => str_plural(strtoupper($this->json['name'])),
				'title' => str_plural(title_case($this->json['name'])),
			],
			'singular' => [
				'camel' => str_singular(camel_case($this->json['name'])),
				'lower' => str_singular(strtolower($this->json['name'])),
				'snake' => str_singular(snake_case($this->json['name'])),
				'studly' => str_singular(studly_case($this->json['name'])),
				'upper' => str_singular(strtoupper($this->json['name'])),
				'title' => str_singular(title_case($this->json['name'])),
			]
		]);
	}
}