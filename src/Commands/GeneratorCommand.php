<?php

namespace Bwi\Rare\Commands;

use Illuminate\Console\Command;

abstract class GeneratorCommand extends Command
{
	/**
	 * Build the directory for the class if necessary.
	 *
	 * @param  string  $path
	 */
	protected function makeDirectory($path)
	{
		if (! $this->filesystem->isDirectory($path)) {
			$this->filesystem->makeDirectory($path, 0777, true, true);
		}
	}

	/**
	 * Get the stub file for the generator.
	 *
	 * @return string
	 */
	abstract protected function getStub();

	/**
	 * Build the file contents
	 *
	 * @return string
	 */
	abstract protected function buildFile();

	/**
	 * Execute the console command.
	 *
	 */
	abstract public function handle();
}
