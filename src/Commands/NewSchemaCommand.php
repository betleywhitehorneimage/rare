<?php

namespace Bwi\Rare\Commands;

use Illuminate\Filesystem\Filesystem;

class NewSchemaCommand extends GeneratorCommand
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'rare:new-schema {name} {schemaOrgType?} {--overwrite}';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Creates a new schema file';

	/**
	 * The path the file will be saved to.
	 *
	 * @var string
	 */
	protected $path = null;

	/**
	 * The filesystem instance.
	 *
	 * @var \Illuminate\Filesystem\Filesystem
	 */
	protected $filesystem;

	/**
	 * Create a new command instance.
	 *
	 */
	public function __construct()
	{
		$this->filesystem = new Filesystem;
		$this->path = config('bwi-rare.schema_path');

		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 */
	public function handle()
	{
		$this->makeDirectory($this->path);

		$filePathAndName = $this->path . '/' . snake_case($this->argument('name')) . '.json';

		if (! $this->option('overwrite')) {
			if ($this->filesystem->exists($filePathAndName)) {
				$this->error('A schema called ' . snake_case($this->argument('name')) . '.json' . ' already exists! Use the --overwrite flag to replace it.');

				return false;
			}
		}

		$this->filesystem->put($filePathAndName, $this->buildFile());

		$this->info('Created new schema file: '. $filePathAndName);
	}

	/**
	 * Get the stub file for the generator.
	 *
	 * @return string
	 */
	protected function getStub()
	{
		return __DIR__.'/../../resources/templates/schema.stub';
	}

	/**
	 * Build the file contents
	 *
	 * @return string
	 */
	protected function buildFile() {
		$stub = $this->filesystem->get($this->getStub());

		$stub = str_replace('SCHEMA_NAME', snake_case($this->argument('name')), $stub);
		$stub = str_replace('SCHEMA_ORG_TYPE', studly_case($this->argument('schemaOrgType')), $stub);

		return $stub;
	}
}
