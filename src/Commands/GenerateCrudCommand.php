<?php

namespace Bwi\Rare\Commands;


use Bwi\Rare\Generator\Builders\BuildController;
use Bwi\Rare\Generator\Builders\BuildModel;
use Bwi\Rare\Generator\Builders\BuildSql;
use Bwi\Rare\Generator\Builders\BuildView;
use Bwi\Rare\SchemaValidator;
use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Spatie\DbDumper\Databases\MySql;
use Symfony\Component\Process\Process;

class GenerateCrudCommand extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'rare:generate {--overwrite}';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Generates the Crud files from your schema.json file';

	/**
	 * The filesystem instance.
	 *
	 * @var \Illuminate\Filesystem\Filesystem
	 */
	protected $filesystem;
	protected $schemaPath;
	protected $debug;

	/**
	 * Execute the console command.
	 */
	public function handle()
	{
		$this->filesystem = new Filesystem;
		$this->schemaPath = config('bwi-rare.schema_path');
		$this->debug = config('bwi-rare.debug');



		if (phpversion() >= 7) {
			$this->backupDatabase();
		} else {
			$this->warn('You are using an old version of php so database backups are not supported. Use php 7 in production.');
		}

		// Load all the schema files
		$schemas = $this->getSchemas();

		foreach ($schemas as $schemaFile) {
			if ($this->validateSchema($schemaFile)) {
				$this->generateSql($schemaFile);
				$this->generateModels($schemaFile);
				$this->generateControllers($schemaFile);
				$this->generateViews($schemaFile);
			}
		}


		// now run php artisan optimize or similar so classes can be found
		$this->codeStyleFixer();

		$this->line('All done!');
	}

	protected function validateSchema($schemaFile)
	{
		$validator = new SchemaValidator($schemaFile);
		$isValid =  $validator->validate();
		$this->ouputMessages($validator);

		return $isValid;
	}

	/**
	 * Writes the create or alter table sql
	 *
	 */
	private function generateSql($schemaFile)
	{
		$this->ouputMessages(new BuildSql($schemaFile));
	}

	/**
	 * Creates the models in the Schema
	 *
	 */
	private function generateModels($schemaFile)
	{
		$this->ouputMessages(new BuildModel($schemaFile));
	}

	/**
	 * Creates the controllers
	 *
	 */
	private function generateControllers($schemaFile)
	{
		$this->ouputMessages(new BuildController($schemaFile));
	}

	/**
	 * Make some sexy views
	 *
	 */
	private function generateViews($schemaFile)
	{
		$this->ouputMessages(new BuildView($schemaFile));
	}

	private function getSchemas()
	{
		return collect($this->filesystem->files($this->schemaPath));
	}

	/**
	 * Outputs the messages to the terminal
	 *
	 */
	protected function ouputMessages($builder) {
		$builder->getMessages()->filter(function($item) {
			return $this->debug || empty($item['debug']);
		})->each(function($item) {
			$this->{$item['type']}($item['text']);
			$this->line('');
		});
	}

	/**
	 * Run Spatie Laravel Backup command
	 */
	private function backupDatabase()
	{
		$path = database_path() . '/backups';
		$filename = config('database.connections.' . config('database.default') . '.database') . '-' . date('Y-m-d-H-i-s') . '.sql';

		if (! $this->filesystem->isDirectory($path)) {
			$this->filesystem->makeDirectory($path);
		}

		MySql::create()
			->setDbName(config('database.connections.' . config('database.default') . '.database'))
			->setUserName(config('database.connections.' . config('database.default') . '.username'))
			->setPassword(config('database.connections.' . config('database.default') . '.password'))
			->dumpToFile($path . '/' . $filename);
		$this->line('Database backed up to ' . $path . '/' . $filename);
	}


	/**
	 * Runs PHP Coding Standards Fixer
	 * https://github.com/FriendsOfPHP/PHP-CS-Fixer
	 */
	private function codeStyleFixer() {
		$this->line('Running PHP Coding Standards Fixer');

		$command = "php-cs-fixer fix";
		$process = new Process($command);
		$process->run();

		if (!$process->isSuccessful()) {
			$this->error('PHP Coding Standards Fixer failed to run. Is it installed and on your path? The files were still successfully generated but might not look as pretty.');
		}
	}

}
