<?php

namespace Bwi\Rare;

use Illuminate\Support\ServiceProvider;

class RareServiceProvider extends ServiceProvider
{

	protected $commands = [
		'Bwi\Rare\Commands\NewSchemaCommand',
		'Bwi\Rare\Commands\GenerateCrudCommand',
	];

	/**
	 * Perform post-registration booting of services.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->publishes([
			__DIR__.'/../config/bwi-rare.php' => config_path('bwi-rare.php'),
			__DIR__.'/../config/.php_cs.dist' => base_path('/') . '.php_cs.dist',
		], 'config');

		$this->mergeConfigFrom(__DIR__.'/../config/bwi-rare.php', 'bwi-rare');

	}


	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->commands($this->commands);
	}
}
