<?php
/*
Reads a JSON schema and allows us to get information about it such as fields,
field properties and relationships
*/

namespace Bwi\Rare;

use DB;

class TableReader
{

	protected $tableName = '';

	public function __construct($tableName)
	{
		$this->tableName = $this->tableName($tableName);

		//print_r($this->tableName . "\r\n");

	}


	/**
	 * Transforms the information about the database into a simple array following the same format as
	 * fields defined in the JSON schema
	 *
	 * @return \Illuminate\Support\Collection
	 */
	public function fields() {
		$tableFields = $this->readDatabaseFields($this->tableName);

		$mapSqlToSchema = [
			'tinyint(1)' => 'bool',
			'int(10) unsigned' => 'foreign-key',
			'int(11)' => 'int',
		];


		$fields = $tableFields->map(function($tableField, $name) use ($mapSqlToSchema) {

			//print_r($tableField);

			// get the current database varchar fields in a format we want
			if (strpos($tableField->Type, 'varchar') === 0) {
				$sqlDataType = 'varchar';

				preg_match('/\d+/', $tableField->Type, $length);
				$sqlLength = $length[0];

				//	$sqlLength = preg_replace_array($tableField->Type, )
			} else {
				$sqlDataType = array_key_exists($tableField->Type, $mapSqlToSchema) ? $mapSqlToSchema[$tableField->Type] : $tableField->Type;
				$sqlLength = null;
			}

			// now build a string of the current database field to compare against
			$databaseField = [
				'dataType' => $sqlDataType,
				'length' => $sqlLength
			];

			// if an actual default has been set then use this
			if ($tableField->Default) {
				$databaseField['default'] = $tableField->Default;
			}

			return $databaseField;
		});


		return $fields;
	}


	/**
	 * Reads all the fields in the database returning them keyed by name
	 *
	 * @return \Illuminate\Support\Collection
	 */
	private function readDatabaseFields() {
		$tableFields = collect(DB::select(DB::raw("SHOW FIELDS FROM " . $this->tableName)))->keyBy('Field')->forget('id');

		return $tableFields;
	}




	/**
	 * Gets name of the database table that will be used for this schema
	 *
	 * @return string
	 * */
	public function tableName($tableName) {
		return DB::getTablePrefix() . str_plural($tableName);
	}

}