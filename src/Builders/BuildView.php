<?php

namespace Bwi\Rare\Generator\Builders;

class BuildView extends Builder
{
	protected $savePath;

	// map datatypes to controls
	protected $controls = [
		'bool' => 'switch',
		'text' => 'textarea',
		'timestamp' => 'datepicker',
		'varchar' => 'input-text',
		'int' => 'input-text',
		'foreign-key' => 'input-text',
	];


	public function __construct($schemaFile)
	{
		parent::__construct($schemaFile);

		$this->savePath = config('bwi-rare.views_path') . '/' . $this->schemaReader->name('plural.lower');
		// make the directories for saving the files if we need them
		$this->makeDirectory($this->savePath);
		$this->makeDirectory($this->savePath . '/rare');

		$this->writeBaseViews();
	}

	private function writeBaseViews()
	{
		$this->writeBaseIndexView();
		$this->writeEditableIndexView();

		$this->writeBaseEditView();
		$this->writeEditableEditView();

	}

	private function writeBaseIndexView()
	{
		$stub = $this->getStub('views/index/base.stub');

		$stub = $this->replaceCommonStrings($stub);

		$fieldsOutput = '';

		// get fields from schema that should be shown on the creation form
		$fieldsOnCreate = $this->schemaReader->fields()->filter(function ($value) {
			if (array_key_exists('showOnCreate', $value)) {
				return $value;
			}
		});

		$fieldsOnCreate->each(function ($item, $key) use (&$fieldsOutput) {
			// load the stub for the control
			if (!array_key_exists('control', $item)) {
				$item['control'] = $this->controls[$item['dataType']];
			}

			// make a dummy field with all the required keys that we can
			// merge our field with so we don’t need to check if they exist
			$requiredFields = ['placeholder' => '', 'length' => ''];

			$item = array_merge($requiredFields, $item);

			// replace strings
			$controlStub = $this->getStub('views/controls/' . $item['control'] . '.stub');

			$find = [
				'FIELD_NAME',
				'FIELD_PLACEHOLDER',
				'FIELD_LENGTH',
				'FIELD_VALUE',
			];

			$replace = [
				$key,
				$item['placeholder'],
				$item['length'],
				'',
			];

			$controlStub = str_replace($find, $replace, $controlStub);


			// now set up the wrapper for the control
			$fieldStub = $this->getStub('views/controls/_control.stub');

			$find = [
				'FIELD_NAME',
				'FIELD_CONTROL',
			];

			$replace = [
				$item['label'],
				$controlStub
			];


			$fieldStub = str_replace($find, $replace, $fieldStub);

			//$fieldsOutput .= str_replace($find, $replace, $controlStub) . "\r\n";


			$fieldsOutput .= $fieldStub;
		});

		$stub = str_replace('SHOW_ON_CREATE_FIELDS', $fieldsOutput, $stub);


		$this->filesystem->put($this->savePath . '/rare/' . 'index.blade.php', $stub);
	}

	private function writeEditableIndexView()
	{
		$outputDestination = $this->savePath . '/' . 'index.blade.php';

		if (!$this->filesystem->exists($outputDestination)) {
			$stub = $this->getStub('views/index/editable.stub');

			$stub = str_replace('BASE_VIEW_PATH', $this->schemaReader->name('plural.lower') . '.rare.index', $stub); // TODO - doesn’t use view path from config so will not match if changed

			$this->filesystem->put($outputDestination, $stub);
		}
	}



	private function writeBaseEditView()
	{
		$stub = $this->getStub('views/edit/base.stub');

		$stub = $this->replaceCommonStrings($stub);

		$fieldsOutput = '';

		$this->schemaReader->fields()->except(['created_at', 'updated_at', 'deleted_at'])->each(function ($item, $key) use (&$fieldsOutput) {
			// load the stub for the control
			if (!array_key_exists('control', $item)) {
				$item['control'] = $this->controls[$item['dataType']];
			}

			// make a dummy field with all the required keys that we can
			// merge our field with so we don’t need to check if they exist
			$requiredFields = ['placeholder' => '', 'length' => ''];

			$item = array_merge($requiredFields, $item);

			// replace strings
			$controlStub = $this->getStub('views/controls/' . $item['control'] . '.stub');
			$valueStub = $this->getStub('views/controls/_value.stub');

			$valueStub = str_replace('SCHEMA_SINGULAR_LOWER', $this->schemaReader->name('singular.lower'), $valueStub);
			$valueStub = str_replace('FIELD_NAME', $key, $valueStub);


			$find = [
				'FIELD_NAME',
				'FIELD_PLACEHOLDER',
				'FIELD_LENGTH',
				'FIELD_VALUE',
			];

			$replace = [
				$key,
				$item['placeholder'],
				$item['length'],
				$valueStub,
			];

			$controlStub = str_replace($find, $replace, $controlStub);


			// now set up the wrapper for the control
			$fieldStub = $this->getStub('views/controls/_control.stub');

			$find = [
				'FIELD_NAME',
				'FIELD_CONTROL',
			];

			$replace = [
				$item['label'],
				$controlStub
			];


			$fieldStub = str_replace($find, $replace, $fieldStub);

			//$fieldsOutput .= str_replace($find, $replace, $controlStub) . "\r\n";


			$fieldsOutput .= $fieldStub;
		});

		$stub = str_replace('SHOW_ON_EDIT_FIELDS', $fieldsOutput, $stub);


		$this->filesystem->put($this->savePath . '/rare/' . 'edit.blade.php', $stub);
	}


	private function writeEditableEditView()
	{
		$outputDestination = $this->savePath . '/' . 'edit.blade.php';

		if (!$this->filesystem->exists($outputDestination)) {
			$stub = $this->getStub('views/edit/editable.stub');

			$stub = str_replace('BASE_VIEW_PATH', $this->schemaReader->name('plural.lower') . '.rare.edit',
				$stub); // TODO - doesn’t use view path from config so will not match if changed

			$this->filesystem->put($outputDestination, $stub);
		}
	}

}