<?php
/**
 * Created by PhpStorm.
 * User: ric
 * Date: 31/08/2016
 * Time: 19:33
 */

namespace Bwi\Rare\Generator\Builders;
use Illuminate\Filesystem\Filesystem;

use Bwi\Rare\SchemaReader;

abstract class Builder
{
	protected $filesystem;

	/**
	 * The path for te schema files.
	 *
	 * @var string
	 */
	protected $schemaPath;
	protected $schemaReader;
	protected $savePath;
	protected $messages = [];

	public function __construct($schemaFile)
	{
		$this->filesystem = new Filesystem;
		$this->schemaPath = config('bwi-rare.schema_path');
		$this->schemaReader = new SchemaReader($schemaFile);
	}

	/**
	 * Build the directory for the class if necessary.
	 *
	 * @param  string  $path
	 */
	protected function makeDirectory($path)
	{
		if (! $this->filesystem->isDirectory($path)) {
			$this->filesystem->makeDirectory($path, 0777, true, true);
		}
	}

	/**
	 * Get the stub file for the generator.
	 *
	 * @return string
	 */
	protected function getStub($name)
	{
		return $this->filesystem->get(__DIR__ . '/../../resources/templates/' . $name);
	}


	/**
	 * Replaces commonly used strings through the system
	 *
	 * @param $stub
	 * @return string
	 */
	protected function replaceCommonStrings($stub) {
		$find = [
			'FILE_CREATION_DATE',

			'SCHEMA_SLUG',

			'SCHEMA_PLURAL_CAMEL',
			'SCHEMA_PLURAL_LOWER',
			'SCHEMA_PLURAL_SNAKE',
			'SCHEMA_PLURAL_STUDLY',
			'SCHEMA_PLURAL_UPPER',
			'SCHEMA_PLURAL_TITLE',

			'SCHEMA_SINGULAR_CAMEL',
			'SCHEMA_SINGULAR_LOWER',
			'SCHEMA_SINGULAR_SNAKE',
			'SCHEMA_SINGULAR_STUDLY',
			'SCHEMA_SINGULAR_UPPER',
			'SCHEMA_SINGULAR_TITLE',
		];

		$replace = [
			date('Y-m-d H:i:s'),

			$this->schemaReader->name('slug'),

			$this->schemaReader->name('plural.camel'),
			$this->schemaReader->name('plural.lower'),
			$this->schemaReader->name('plural.snake'),
			$this->schemaReader->name('plural.studly'),
			$this->schemaReader->name('plural.upper'),
			$this->schemaReader->name('plural.title'),

			$this->schemaReader->name('singular.camel'),
			$this->schemaReader->name('singular.lower'),
			$this->schemaReader->name('singular.snake'),
			$this->schemaReader->name('singular.studly'),
			$this->schemaReader->name('singular.upper'),
			$this->schemaReader->name('singular.title'),
		];

		return str_replace($find, $replace, $stub);
	}

	/**
	 * Get the messages
	 *
	 * @return Illuminate\Support\Collection
	 */
	public function getMessages(){
		return collect($this->messages);
	}


	/**
	 * Output messages to the cli
	 */
	protected function info($message)
	{
		$this->messages[] = ['type' => 'info', 'text' => $message];
	}

	protected function error($message)
	{
		$this->messages[] = ['type' => 'error', 'text' => $message];
	}

	protected function debug($message)
	{
		$this->messages[] = ['type' => 'error', 'text' => $message, 'debug' => true];
	}
}