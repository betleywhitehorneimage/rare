<?php
/**
 * Created by PhpStorm.
 * User: ric
 * Date: 31/08/2016
 * Time: 19:33
 */

// TODO this should be separated into at least two classes. One that reads all the schemas and validates them, it then
// creates an object for each schema (second file) which writes the model. Maybe actually a class is run before anything
// else that does the validation?

namespace Bwi\Rare\Generator\Builders;

class BuildModel extends Builder
{
	protected $savePath;
	protected $imports = [];
	protected $traits = [];
	protected $methods = [];
	protected $properties = ['public' => [], 'protected' => []];
	protected $dates = [];

	public function __construct($schemaFile)
	{
		parent::__construct($schemaFile);

		$this->savePath = config('bwi-rare.models_path') . '/';

		// make the directories for saving the files if we need them
		$this->makeDirectory($this->savePath);
		$this->makeDirectory($this->savePath . '/Rare');

		// copy the base RareModel that all others extend
		$this->filesystem->copy(__DIR__ . '/../../resources/templates/models/rare-base-model.stub', $this->savePath . '/Rare/RareModel.php');

		$this->writeBaseModel();
		$this->writeEditableModel();
	}


	private function writeBaseModel() {
		$stub = $this->getStub('models/rare-model.stub');
		$stub = $this->replaceCommonStrings($stub);

		// guarded fields
		$stub = $this->guardedFields($stub);
		$stub = $this->activityLoggedFields($stub);

		//exit();
		// schema.org type
		$stub = str_replace('SCHEMA_ORG_TYPE', $this->schemaReader->schemaOrgType(), $stub);

		// soft deletes
		$this->softDeletes();

		// soft deletes
		$this->sluggable();

		// created_at and modified_at timestamps TODO - do we actually need to add these to the property or does Laravel
		// assume they exist even when using custom dates
		//$this->timestamps($this->schemaReader);

		// Any custom timestamps that should be added to the $date property
		$this->additionalTimestamps();




		// get all the properties
	//	$this->schemaProperties();

		// write file
		$stub = $this->writeImports($stub);
		$stub = $this->writeTraits($stub);
		$stub = $this->writeDates($stub);
		$stub = $this->writeProperties($stub);
		$stub = $this->writeMethods($stub);
		$stub = $this->writeValidationRules($stub);
		$stub = $this->writeRelationships($stub);
		$stub = $this->writeSchemaFields($stub);



		$stub = str_replace('FILE_CREATION_DATE', date('Y-m-d H:i:s'), $stub);

		$this->filesystem->put($this->savePath . 'Rare/' . $this->schemaReader->name('singular.studly') . '.php', $stub);


	}

	private function writeEditableModel() {
		$outputDestination = $this->savePath . $this->schemaReader->name('singular.studly') . '.php';

		if (!$this->filesystem->exists($outputDestination)) {
			$stub = $this->getStub('models/model.stub');

			$stub = $this->replaceCommonStrings($stub);

			$this->filesystem->put($outputDestination, $stub);
		}
	}


	public function writeValidationRules($stub)
	{
		$validationFields = $this->schemaReader->validatedFields();

		// using map returns a new collection meaning we still have the original with the messages key to work on
		$rules = $validationFields->map(function($item, $key) {
			return '\'' . $key . '\' => \'' . $item['validation']['rules'] . '\'';
		})->implode(', ' . "\r\n");

		$validationStub = $this->getStub('models/properties/validation.stub');

		$validationStub = str_replace('VALIDATION_RULES', $rules, $validationStub);

		$messages = $validationFields->filter(function($item) {
			if (is_array($item['validation'])) {
				if (array_key_exists('messages', $item['validation'])) {
					return $item;
				}
			}
		})->transform(function($item, $field) {
			$messages = collect($item['validation']['messages'])->transform(function($item, $key) use ($field) {
				return '\'' . $field . '.' . $key . '\' => \'' . $item . '\'';
			})->implode(',' . "\r\n");

			return $messages;
		})->implode(', ');

		$validationMessagesStub = $this->getStub('models/properties/validation-messages.stub');
		$validationMessagesStub = str_replace('VALIDATION_MESSAGES', $messages, $validationMessagesStub);

		$validationStub = str_replace('VALIDATION_MESSAGES', $validationMessagesStub, $validationStub);

		$stub = str_replace('MODEL_VALIDATION', $validationStub, $stub);

		return $stub;
	}

	protected function getSchemas()
	{
		$schemas = $this->filesystem->files($this->schemaPath);

		if (!$schemas) {
			// $this->error('You have no schema files. Run rare:new-schema {name} {schema.org name to generate}');
			// TODO how do we print this to the console or pass it back to the command class?
			//$this->output->writeln("<error>fggfhfg</error>");
			return false;
		}

		return collect($schemas);
	}

	/**
	 * @param $this->schemaReader
	 * @param $stub string
	 * @return mixed|string
	 */
	protected function guardedFields($stub)
	{
		if ($guardedFields = $this->schemaReader->guardedFields()) {
			$guardedFields = $guardedFields->transform(function ($item) {
				return '\'' . $item . '\'';
			})->implode(', ');

			return str_replace('GUARDED_FIELDS', $guardedFields, $stub);
		}

		// return empty array - this makes all fields
		return str_replace('GUARDED_FIELDS', '', $stub);
	}

	/**
	 * @param $this->schemaReader
	 * @param $stub string
	 * @return mixed|string
	 */
	protected function activityLoggedFields($stub)
	{
		if ($activityLoggedFields = $this->schemaReader->fields(false)) {
			$activityLoggedFields = $activityLoggedFields->keys()->transform(function($item) {
				return '\'' . $item . '\'';
			})->implode(', ');

			return str_replace('ACTIVITY_LOGGED_FIELDS', $activityLoggedFields, $stub);
		}

		// return empty array - this makes all fields
		return str_replace('ACTIVITY_LOGGED_FIELDS', '', $stub);
	}



	private function softDeletes()
	{
		// TODO define these in another file and read them in
		if ($this->schemaReader->softDeletes()) {
			$this->imports[] = 'use Illuminate\Database\Eloquent\SoftDeletes;';
			$this->traits[] = 'use SoftDeletes;';
		}
	}



	private function sluggable()
	{
		// TODO define these in another file and read them in


		if ($sluggableFields = $this->schemaReader->getTrait('sluggable')) {
			$this->imports[] = 'use Spatie\Sluggable\HasSlug;';
			$this->imports[] = 'use Spatie\Sluggable\SlugOptions;';
			$this->traits[] = 'use HasSlug;';

			$sluggableFields = collect($sluggableFields)->filter(function($item) {
				// make sure the sluggable field is in the schema’s fields
				if ($this->schemaReader->fields()->has($item)) {
					return $item;
				} else {
					$this->error('Sluggable field ' . $item . ' is not in the schema ' . $this->schemaReader->name('singular.studly') . '.json and was removed from the generated file.');
				}
			})->transform(function($item) {
				return '\'' . $item . '\'';
			})->implode(', ');

			if ($sluggableFields) {
				$stub = $this->getStub('models/traits/sluggable.stub');
				$this->methods[]  = str_replace('SLUGGABLE_FIELDS', $sluggableFields, $stub);
			}
		}
	}

	/**
	 * TODO is this needed or will Laravel add them anyway, even if we have a $dates property
	 *
	 * @param $this->schemaReader
	 *
	 * Adds timestamps from the schema to the model’s $dates property
	 */
	/*private function timestamps($this->schemaReader) {
		$this->dates = array_merge($this->dates, $this->schemaReader->dates());
	}*/

	/**
	 * @param $this->schemaReader
	 *
	 * Adds timestamps from the schema to the model’s $dates property
	 */
	private function additionalTimestamps() {
		$this->dates = array_merge($this->dates, $this->schemaReader->dates());
	}

	private function writeImports($stub)
	{
		$imports = collect($this->imports)->implode("\r\n");

		return str_replace('MODEL_IMPORTS', $imports, $stub);
	}

	private function writeTraits($stub)
	{
		$traits = collect($this->traits)->implode("\r\n");

		return str_replace('MODEL_TRAITS', $traits, $stub);
	}

	private function writeMethods($stub)
	{
		$methods= collect($this->methods)->implode("\r\n\r\n");

		return str_replace('MODEL_METHODS', $methods, $stub);
	}


	/**
	 * @param $this->schemaReader
	 * @param $stub
	 * @return mixed|string
	 *
	 * Get and combine properties from the schema and settings in the model
	 */
	private function writeProperties($stub)
	{
		$propertiesStub = $this->getStub('database/properties/property.stub');
		$propertiesOutput = '';

		// read properties in the schema file
		$schemaProperties = $this->schemaReader->properties();

		$schemaProperties->filter()->each(function ($item, $key) {
			$this->addProperty($key, $item);
		});

		$properties = collect($this->properties);

		$properties->each(function ($item, $key) use ($propertiesStub, &$propertiesOutput) {
			$access = $key;

			$properties = collect($item);

			$properties->each(function ($item, $key) use ($propertiesStub, &$propertiesOutput, $access) {
				$find = ['PROPERTY_ACCESS', 'PROPERTY_NAME', 'PROPERTY_VALUE'];
				$replace = [$access, $key, $item ? 'true' : 'false'];

				$propertiesOutput .= str_replace($find, $replace, $propertiesStub) . "\r\n";
			});

		});


		return str_replace('MODEL_PROPERTIES', $propertiesOutput, $stub);
	}

	private function writeDates($stub)
	{
		$datesStub = $this->getStub('database/properties/dates.stub');

		$dates = collect($this->dates)->transform(function ($item, $key) {
			return '\'' . $item . '\'';
		})->implode(', ');

		$datesStub = str_replace('MODEL_DATES', $dates, $datesStub);

		return str_replace('MODEL_DATES', $datesStub, $stub);
	}


	private function addProperty($access, $property)
	{
		$this->properties[$access][key($property)] = $property[key($property)];
	}

	/**
	 * @param $this->schemaReader
	 * @param $stub
	 * @return mixed|string
	 *
	 * Write all of our relationships
	 */
	private function writeRelationships($stub)
	{
		$schemaRelations = $this->schemaReader->relationships();

		$relationshipsOutput = '';

		if ($schemaRelations) {

			$schemaRelations->each(function ($item, $key) use(&$relationshipsOutput) {

				$relationshipType = (array) $item['type'];
				$relationshipStub = $this->getStub('models/relationships/' . str_replace('_', '-', snake_case($relationshipType[0])) . '.stub');

				$relationshipsOutput .= str_replace(
					['RELATIONSHIP_NAME', 'RELATIONSHIP_MODEL', 'RELATIONSHIP_TROUGH_MODEL', 'MORPH_RELATIONSHIP'],
					[
						$key,
						isset($item['schema']) ? 'App\Models\\' . str_singular(studly_case($item['schema']->name())) : null,
						isset($relationshipType[1]) ? 'App\Models\\' . str_singular(studly_case($relationshipType[1])) : null,
						isset($relationshipType[1]) ? $relationshipType[1] : null
					],
					$relationshipStub) . "\r\n";
			});
		}

		return str_replace('MODEL_RELATIONSHIPS', $relationshipsOutput, $stub);
	}

	// TODO writeSchemaType - gets the Schema.org type for the table

	/**
	 * Writes the methods for getting the Schema.org property for the field
	 *
	 * @param $this->schemaReader
	 * @param $stub
	 * @return mixed|string
	 */
	private function writeSchemaFields($stub)
	{
		$schemaFields = $this->schemaReader->fields()->filter(function ($item, $key) {
			if (array_key_exists('schemaOrgProperty', $item)) {
				return true;
			}
		})->transform(function ($item, $key) {
				$fieldStub = $this->getStub('models/schema-org/field.stub');

				$fieldProperty = str_replace([
						'FIELD_NAME',
						'FIELD_SCHEMA_ORG_PROPERTY'
					],
					[
						$key,
						$item['schemaOrgProperty']
					], $fieldStub);

				return $fieldProperty;

		})->implode(', ');


		$schemaFields = 'protected $schemaOrgProperties = [' . $schemaFields . '];';

		return str_replace('SCHEMA_ORG_PROPERTIES', $schemaFields, $stub);

	}


}