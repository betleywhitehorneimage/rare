<?php

namespace Bwi\Rare\Generator\Builders;

class BuildController extends Builder
{
	protected $savePath;


	public function __construct($schemaFile)
	{
		parent::__construct($schemaFile);

		$this->savePath = config('bwi-rare.controllers_path');

		// make the directories for saving the files if we need them
		$this->makeDirectory($this->savePath);
		$this->makeDirectory($this->savePath . '/Rare');

		$this->writeBaseController();
		$this->writeEditableController();
	}

	private function writeBaseController()
	{
		$stub = $this->getStub('controllers/rare-controller.stub');
		$stub = $this->replaceCommonStrings($stub);

		$this->filesystem->put($this->savePath . '/Rare/' . $this->schemaReader->name('singular.studly') . 'Controller.php', $stub);

	}

	private function writeEditableController()
	{
		$outputDestination = $this->savePath . $this->schemaReader->name('singular.studly') . 'Controller.php';

		if (!$this->filesystem->exists($outputDestination)) {
			$stub = $this->getStub('controllers/controller.stub');

			$stub = $this->replaceCommonStrings($stub);

			$this->filesystem->put($outputDestination, $stub);
		}
	}
}