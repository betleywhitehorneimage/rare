<?php

namespace Bwi\Rare\Generator\Builders;

use Bwi\Rare\TableReader;
use DB;
use Illuminate\Support\Facades\Schema;

class BuildSql extends Builder
{
	public function __construct($schemaFile)
	{
		// Turns off the strict mode in the DB connection
		$connection_name = config('database.default');
		config(['database.connections.'.$connection_name.'.strict' => false]);

		parent::__construct($schemaFile);
		$this->savePath = database_path('sql/');
		$this->makeDirectory($this->savePath);
		$this->createOrAlterTable();
	}

	/**
	 *  Checks if we need to write a create or alter table script
	 */
	private function createOrAlterTable() {
		$tableName = $this->schemaReader->name('plural.lower');

		if (Schema::hasTable($tableName)) {
			$this->writeAlterTableSql();
		} else {
			$this->writeCreateTableSql();
		}
	}

	/**
	 * Writes alter table sql
	 * It gets the columns currently in the table and compares them to those in the schema.json25
	 *
	 * TODO do we have to remove timestamp fields? or check for them? we probably do
	 * TODO - ADD field AFTER - so the fields are in a sexy order in the database
	 */
	private function writeAlterTableSql() {
		$stub = $this->getStub('database/alter-table.stub');
		$stub = str_replace('TABLE_NAME', $this->schemaReader->tableName(), $stub);

		$tableReader = new TableReader($this->schemaReader->name('plural.lower'));

		$schemaFields = $this->schemaReader->fields();
		$tableFields = $tableReader->fields();

		// New fields added to the schema
		$addFields = $schemaFields->diffKeys($tableFields)->map(function ($schemaField, $name) {
			$schemaFieldSql = $this->writeFieldSql($schemaField, $name);

			return $schemaFieldSql;
		});


		// remove any new fields from the comparison
		if (!$addFields->isEmpty()) {
			$fieldsToCompare = $schemaFields->except($addFields->keys()->toArray());
		} else {
			$fieldsToCompare = $schemaFields;
		}

		$addFields = $addFields->map(function ($sql, $name) {
			$stub = $this->getStub('database/add-column.stub');

			return str_replace('FIELD', $sql, $stub);
		})->implode(', ');


		$modifiedFields = $fieldsToCompare->map(function ($schemaField, $name) use ($tableFields) {
			$schemaFieldSql = $this->writeFieldSql($schemaField, $name);
			$tableFieldSql = $this->writeFieldSql($tableFields[$name], $name);

			if ($schemaFieldSql !== $tableFieldSql) {
				return $schemaFieldSql;
			}

		})->filter()->map(function ($sql, $name) {
			$stub = $this->getStub('database/modify-column.stub');

			return str_replace('FIELD', $sql, $stub);
		})->implode(', ');


		// fields removed from the schema
		$droppedFields = $tableFields->diffKeys($schemaFields)->map(function ($sql, $name) {
			$stub = $this->getStub('database/drop-column.stub');

			return str_replace('FIELD', $name, $stub);
		})->implode(', ');

		// we need commas between each of the above but might not have changed each type
		$changes = [];

		if ($addFields) {
			$changes[] = $addFields;
		}

		if ($modifiedFields) {
			$changes[] = $modifiedFields;
		}

		if ($droppedFields) {
			$changes[] = $droppedFields;
		}

		if ($changes) {
			$changes = collect($changes)->implode(', ');

			$stub = str_replace('CHANGES', $changes, $stub);

			$stub = $this->replaceCommonStrings($stub);

			$this->info('Altered SQL for ' . $this->schemaReader->tableName());
			$this->debug($stub);

			DB::statement($stub);
		}
	}

	/**
	 * Writes create table sql
	 **/
	private function writeCreateTableSql()
	{
		$stub = $this->getStub('database/create-table.stub');

		$stub = str_replace('TABLE_NAME', $this->schemaReader->tableName(), $stub);

		$fields = $this->schemaReader->fields();


		$fields = $fields->map(function ($field, $name) {
			return $this->writeFieldSql($field, $name);
		})->implode(', ');


		$stub = str_replace('TABLE_FIELDS', $fields, $stub);

		$this->info('Created SQL for ' . $this->schemaReader->tableName());
		$this->debug($stub);

		DB::statement($stub);
	}

	private function writeFieldSql($field, $name) {
		// TODO see if we are using a control that wants a default datatype
		$stub = $this->getStub('database/types/' . $field['dataType'] . '.stub');

		$stub = str_replace('FIELD_NAME', $name, $stub);

		if (array_key_exists('length', $field)) {
			$stub = str_replace('FIELD_LENGTH', $field['length'], $stub);
		}

		$stub = $this->writeDefaultValue($stub, $field);

		return $stub;
	}

	private function writeDefaultValue($stub, $field) {
		/*
		 * TODO - better way of handling this?
		 * There are certain values that are special defaults that we don’t want to add to default template
		 * These might include values that shouldn’t be quoted
		 * */
		$excludedDefaults = [
			'null',
			'CURRENT_TIMESTAMP'
		];

		// we have supplied a default
		if (array_key_exists('default', $field) && !in_array($field['default'], $excludedDefaults)) {
			$defaultStub = $this->getStub('database/properties/default.stub');
			$defaultStub = str_replace('FIELD_DEFAULT', $field['default'], $defaultStub);

			return str_replace('FIELD_DEFAULT', $defaultStub, $stub);
		}

		/*
		 * Some fields should use a special template for their default. Either because MySQL gives the field a default
		 * default which we want to override or because the application will be expecting a certain default. For example
		 * MySQL will set a varchar null if no default is defined
		 * [$dataType => $template]
		 * */
		$mysqlDefaults = [
			'varchar' => 'null',
			'timestamp' => 'timestamp'
		];

		if (array_key_exists($field['dataType'], $mysqlDefaults)) {
			$defaultStub = $this->getStub('database/properties/default-' . $mysqlDefaults[$field['dataType']] . '.stub');

			return str_replace('FIELD_DEFAULT', $defaultStub, $stub);
		}

		// no default of any type needed so return nothing
		return str_replace('FIELD_DEFAULT', '', $stub); // return stub without any value
	}

}